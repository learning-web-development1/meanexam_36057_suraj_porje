const express = require('express')
const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken')
const config = require('./config')
const cors = require('cors')

// morgan: for logging
const morgan = require('morgan')

// swagger: for api documentation



// routers
const adminRouter = require('./routes/admin')
const employeeRouter = require('./routes/employee')

const app = express()
app.use(cors())

app.use(bodyParser.json())
app.use(morgan('combined'))

function getUserId(request, response, next) {

  if (request.url == '/admin/signin'
      || request.url == '/admin/signup'
      || request.url == '/employee/signup'
      || request.url == '/employee/signin')  {
   
    next()
  } else {

    try {
      const token = request.headers['token']
      const data = jwt.verify(token, config.secret)

     
      request.userId = data['id']

      
      next()
      
    } catch (ex) {
      response.status(401)
      response.send({status: 'error', error: 'protected api'})
    }
  }
}

app.use(getUserId)


app.use(express.static('images/'))

// add the routes
app.use('/admin',adminRouter)
app.use('/employee',employeeRouter)

// default route
app.get('/', (request, response) => {
  response.send('welcome to my application')
})

app.listen(3000, '0.0.0.0', () => {
  console.log('server started on port 3000')
})

            